import './App.scss';
import AppRoutes from './Components/AppRoutes/AppRoutes';

function App() {
  return (
    <div className="App">
      <AppRoutes />
    </div>
  );
}

export default App;
