import cn from "classnames";
import PropTypes from "prop-types";

import "./Button.scss";

const Button = ({ type, optionalClassNames, src, children, onClick }) => {
    return (
        <div className={cn("button-wrapper",optionalClassNames)}>
            <button type={type} onClick={onClick}>
                {src && <img src={src} alt="icon"></img>}
                {children}
            </button>
        </div>
    )
}

Button.defaultProps = {
    type: "button"
  };

Button.propTypes = {
    type: PropTypes.string,
    optionalClassNames: PropTypes.string,
    src: PropTypes.string,
    onClick: PropTypes.func,
    product: PropTypes.object
  };

export default Button;