import { useState } from 'react';
import { useSelector } from "react-redux";

import Button from "../Button/Button";
import "./Checkout.scss";
import ModalCheckout from "../Modal/ModalChecout/ModalCheckout";

const Сheckout = () => {
    const [isOpenModalCheckout, setisOpenModalCheckout] = useState(false);

    const toggleOpenModalCheckout = () => {
        setisOpenModalCheckout(!isOpenModalCheckout);
    }

    const cartProducts = useSelector(state => state.main.cartProducts);
    let sumPrice = cartProducts.reduce((sum, item) => sum + Number(item.price.replace(/ /g,'')) * item.count, 0);
    sumPrice = sumPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    
    return(
        <div className="checkout row">
            <div className="checkout__sum">{sumPrice} грн.</div>
            <Button onClick={ () => toggleOpenModalCheckout() }>Оформити замовлення</Button>
            <ModalCheckout isOpenModalCheckout={isOpenModalCheckout} toggleOpenModalCheckout={toggleOpenModalCheckout}/>
        </div>
    )
}

export default Сheckout;