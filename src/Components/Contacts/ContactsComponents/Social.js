import "../Contacts.scss";

import Icon from "../../Icon/Icon";

const Social = () => {
    return(
        <div className="social row">
            <a href="https://t.me/ElectronicBikeKit">
                <Icon src="/images/icons/telegram.png" alt="telegram" width='48px' height="48px"/>
            </a>
            <a href="viber://chat?number=%2B380505621868">
                <Icon src="/images/icons/viber.png" alt="viber" width='48px' height="48px"/>  
            </a>
        </div>
    )
}

export default Social;