import PropTypes from "prop-types";
import { Formik, Form } from 'formik';
import {useSelector, useDispatch} from "react-redux";
import {removeCartProducts} from "../../../store/mainSlice";

import "../Formik.scss";

import FieldWrapper from './FieldWrapper';
import PhoneField from "./PhoneField";
import Button from "../../Button/Button";
import {checkoutSchema} from "../validation";

const FormikWrapper = ({ toggleOpenModalCheckout }) => {
  const cartProducts = useSelector(state => state.main.cartProducts);
  const dispatch = useDispatch();

    return(
      <div className = "formic-wrapper">
        <Formik 
            initialValues={{
                firstName: '',
                lastName: '',
                age: '',
                phone: '',
                address: '',
            }}
            validationSchema={checkoutSchema}
            onSubmit={
                values => {
                    console.log("Інформація про клієнта:",values);
                    console.log("Куплені товари:",cartProducts);
                    dispatch(removeCartProducts());
                    toggleOpenModalCheckout();
                }
            }>
            {({ errors, touched }) => (
              <Form className="formic-wrapper__form">
                <FieldWrapper name="firstName" children="Ім'я" />
                <FieldWrapper name="lastName" children="Прізвище" />
                <FieldWrapper name="age" children="Ваш вік" />
                <PhoneField name="phone" children="Мобільний телефон"/>
                <FieldWrapper name="address" children="Адреса доставки" />  
                <Button 
                    type="submit"
                    children="Замовлення підтверджую" 
                />
              </Form>   
            )}      
        </Formik>
      </div>   
    )
}

FormikWrapper.propTypes = {
  toggleOpenModalCheckout: PropTypes.func
};

export default FormikWrapper;