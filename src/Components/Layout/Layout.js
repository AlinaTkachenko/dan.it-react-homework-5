import { Outlet } from 'react-router-dom';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';

const Layout = () => {
    return (
        <div className='wrapper'>
            <Header />
            <div className='main content'>
                <Outlet />
            </div>
            <Footer />
        </div>
    )
}

export default Layout;