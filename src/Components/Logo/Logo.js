import {Link} from 'react-router-dom';

import "./Logo.scss";

const Logo = () => {
    return (
        <Link to='/'>
            <div className="logo">
                <span className="logo__main">EBK</span>
                <span className="logo__full-name">Electronic Bike<br />Kit's</span>
            </div>
        </Link>
    )
}

export default Logo;