import PropTypes from "prop-types";
import { useDispatch,useSelector } from "react-redux";
import { toggleCartProduct,toggleModal } from "../../../store/mainSlice";

import ModalWrapper from "./../ModalComponents/ModalWrapper"
import Modal from "./../ModalComponents/Modal";
import ModalHeader from "./../ModalComponents/ModalHeader";
import ModalBody from "./../ModalComponents/ModalBody";
import ModalImg from "./../ModalComponents/ModalImg";
import ModalFooter from "./../ModalComponents/ModalFooter";

const ModalCart = () => {
    const dispatch = useDispatch();
    const isOpenModalCart = useSelector(state => state.main.isOpenModalCart);
    const currentCard = useSelector(state => state.main.currentCard);    
    const {src, title, price, } = currentCard;
    return (  
        <ModalWrapper isOpen={isOpenModalCart}>
            <Modal closeModal={() => dispatch(toggleModal("modalCart"))}>
                <ModalImg src={src} alt={title}></ModalImg>
                <ModalHeader>{title}</ModalHeader>
                <ModalBody optionalClassName="body-price">{price} грн.</ModalBody>
                <ModalFooter 
                    firstText="Додати до кошика" 
                    firstClick={() => {
                        dispatch(toggleCartProduct({currentCard: currentCard, action: "plus"}));
                        dispatch(toggleModal("modalCart"));
                    }}/>
            </Modal>
        </ModalWrapper>
    )
}

ModalCart.propTypes = {
    isOpenModalCart: PropTypes.bool,
    toggleModalCart: PropTypes.func,
  };

export default ModalCart;