import PropTypes from 'prop-types';

const ColorsProductCard = ({color}) => {
    return (
        <div className="product-card__color">
            <span style={{ backgroundColor: color}}></span>
        </div>
    )
}

ColorsProductCard.propTypes = {
    color: PropTypes.string
  };

export default ColorsProductCard;