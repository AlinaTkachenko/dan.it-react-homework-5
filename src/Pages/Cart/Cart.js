import {useSelector} from "react-redux";

import "./Cart.scss";

import Title from "../../Components/Title/Title";
import CartProducts from "./CartComponents/CartProducts";
import Checkout from "../../Components/Checkout/Checkout";

const Cart = () => { 
    const cartProducts = useSelector(state => state.main.cartProducts);

    return (
        <div className="cart">
            <div className="interior-content">
                <Title>Кошик</Title>
                <CartProducts />
                {cartProducts.length > 0 && <Checkout />}
            </div>
        </div>
    )
}

export default Cart; 