import { useNavigate } from "react-router-dom";

import "./Product.scss";

import Button from "../../Components/Button/Button";

const Product = () => {

    const navigate = useNavigate();

    const goBack = () => {
        navigate(-1);
    }

    return (
        <div className="product">
            <div className="interior-content">
                <Button src="/images/icons/arrow.png" optionalClassNames="icon back" onClick={goBack}></Button>
            </div>
        </div>
    )
}

export default Product;